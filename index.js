"use strict";

class Normalise {
    static channel(c) {
        if (c && c.indexOf('#') === 0) {
            return c.substr(1).toLowerCase();
        }
        return (c || '').toLowerCase();
    }

    static user(u) {
        if (u && u.hasOwnProperty("username")) {
            u.username = (u.username || '').toLowerCase();
            return u;
        }

        return (u || "").toLowerCase();
    }
}

module.exports = Normalise;
